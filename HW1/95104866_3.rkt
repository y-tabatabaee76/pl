;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname 95104866_3) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f () #f)))
(define (poly-comp coef x)
  (cond
    [(null? coef) 0]
    [else
      (+ (car coef)
         (* x (poly-comp (cdr coef) x)))]
  ) 
)

(define (approx-integral coef lb ub n)
  (cond
    [(= n 1) (* (poly-comp coef (/ (+ lb ub) 2)) (- ub lb))]
    [else
      (+ (approx-integral coef lb (/ (+ ub (* (- n 1) lb)) n) 1)
         (approx-integral coef (/ (+ ub (* (- n 1) lb)) n) ub (- n 1)))]
  )  
)  

(define (main x)
  (approx-integral
   (car x) (car (cdr x)) (car (cdr (cdr x))) (car (cdr (cdr (cdr x)))))
)