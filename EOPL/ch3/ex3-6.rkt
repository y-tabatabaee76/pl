; EOPL-3.6
#lang eopl

;1-  adding minus to grammer
(define grammar
  '((program (expression) a-program)
     ...
    (expression ("minus" "(" expression ")") minus-exp)
    ...
    ))

;2-  minus abstract syntax
Expression ::= minus(Expression)
               (minus-exp (exp1)
                      
;3- specification of minus-exp
(value-of (minus-exp exp1) ρ) = 
(expval->num (- 0 (num-val (value-of exp1 ρ))))                        

;4- Adding constructor to expression datatype
(define-datatype expression expression?
  ...
  (minus-exp
   (exp1 expression?))
  ...
 )
                                            
;5- adding minus to value-of
(define value-of
  (lambda (exp env)
    (cases expression exp
          ...
           (minus-exp (exp1)
                      (let ((val1 (value-of exp1 env)))
                        (let ((num1 (expval->num val1)))
                          (num-val
                           (- 0 num1)))))
          ...
           )))



